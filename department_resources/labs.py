import sys, random, json, requests
from bs4 import BeautifulSoup

from department_resources.meta import DepartmentMeta


class DepartmentLabs(DepartmentMeta):

    def __init__(self, queries_path):
        super().__init__(queries_path)

    def update(self):
        """
        Updates the source information used to answer questions (i.e. department chair's name)
        :return: True if successful, False otherwise (?)
        """
        try:
            self.scrape_and_dump_json()
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror))
            return False
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return True

    def scrape_and_dump_json(self):
        self.scrape_and_dump_labs()


    def scrape_and_dump_general(self):
        pass

    def scrape_and_dump_labs(self):
        url = "http://frank.ored.calpoly.edu/LabList"
        myRequest = requests.get(url)
        soup = BeautifulSoup(myRequest.text,"html.parser")

        questions = {}

        for h2 in soup.find_all('h2'):

           if(h2.has_attr("id")):
              lab = h2.string
              question  = "What is in " + lab + "?"
              answer = h2.findNext("p").string.replace("\n", "")
              questions[question] = answer

              ayy = lab.split("(")
              labName =  ayy[0].strip()
              labNum = ayy[1][:-1]

              question  = "What is in " + labNum + "?"
              questions[question] = answer

              question  = "What is in the" + labName + "?"
              questions[question] = answer


        with open('department_resources/labs.json', 'w') as fp:
            json.dump(questions, fp, sort_keys=True, indent=4)