import sys, random, json, requests, re
from bs4 import BeautifulSoup
from department_resources.meta import DepartmentMeta


class DepartmentNewsClubs(DepartmentMeta):


    NEWS_URL = "https://csc.calpoly.edu/news/"
    CLUBS_URL = "https://csc.calpoly.edu/clubs/"
    CAL_POLY_URL = "https://csc.calpoly.edu"


    JSON_PATHS = ("department_resources/news.json", "department_resources/clubs.json")

    def __init__(self, queries_path):
        super().__init__(queries_path)
        super().update()

    def update(self):
        """
        Updates the source information used to answer questions (i.e. department chair's name)
        :return: True if successful, False otherwise (?)
        """
        try:
            self.scrape_and_dump_json()
            super().update()
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror))
            return False
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return True

    def fill_variables(self):
        for question_key in self.questions.keys():
            response = self.questions[question_key][self.RESPONSE_INDEX]
            vars = re.findall(self.VARIABLE_REGEX, response)
            for var in vars:
                var_name = var[1:-1]
                if var_name in self.data.keys():
                    response = re.sub('\[' + var_name + '\]', self.data[var_name], response)
                elif self.DEBUG:
                    print("variable substitution failed for: " + var + ' in \"' +  response + '\"', file=sys.stderr)
            self.questions[question_key][self.RESPONSE_INDEX] = response

    def scrape_and_dump_json(self):
        self.scrape_and_dump_news()
        self.scrape_and_dump_clubs()
        self.scrape_and_dump_student_count()

    def scrape_and_dump_news(self):


        myRequest = requests.get(self.NEWS_URL)
        soup = BeautifulSoup(myRequest.text, "html.parser")
        table = soup.find(attrs={"id": "news_archive"})

        monthsList = []
        for month_buttons in table.find_all('ul'):
            for months in month_buttons.find_all('a'):
                monthsList.append(months.get("href"))

        final_news_json = {}
        all_latest_news_json = []
        news_archive = soup.find(attrs={"id": "latest_articles"})
        for news in news_archive.find_all('p'):
            if (news.find('a') and news.find('i')):
                news_json = {}
                news_json['ARTICLE'] = news.find('a').get_text()
                news_json['URL'] = news.find('a').get("href")
                date = news.find('i').get_text()
                date_split = date.split(" ")
                news_json['DATE'] = date
                news_json['MONTH'] = date_split[0]
                news_json['DAY'] = date_split[1][:-1]
                news_json['YEAR'] = date_split[2]
                all_latest_news_json.append(news_json)
        
        all_news_json = []

        for month in monthsList:
            myRequest = requests.get(self.CAL_POLY_URL + month)
            soup = BeautifulSoup(myRequest.text, "html.parser")
            news_archive = soup.find(attrs={"id": "mainLeftFull"})
            for news in news_archive.find_all('p'):
                if (news.find('strong') != None and news.find('strong').find('a')):
                    news_json = {}
                    news_json['ARTICLE'] = news.find('strong').get_text()
                    news_json['URL'] = news.find('strong').find('a').get("href")
                    date = news.find('em').get_text()
                    date_split = date.split(" ")
                    news_json['DATE'] = date
                    news_json['MONTH'] = date_split[0]
                    news_json['DAY'] = date_split[1][:-1]
                    news_json['YEAR'] = date_split[2]
                    all_news_json.append(news_json)

        final_news_json['news archive'] = all_news_json
        final_news_json['latest news'] = all_latest_news_json
        with open('department_resources/news.json', 'w') as fp:
            json.dump(final_news_json, fp, sort_keys=True, indent=4)
            

    def scrape_and_dump_clubs(self):

        myRequest = requests.get(self.CLUBS_URL)
        soup = BeautifulSoup(myRequest.text, "html.parser")
        table = soup.find(attrs={"id": "mainLeftFull"})

        clubs = table.find_all('td')
        club_names = [x.get_text() for x in clubs[::2]]
        club_websites = [x.get_text() for x in clubs[1::2]]

        all_club_json = []
        for ndx in range(len(club_names)):
            single_club_json = {}
            single_club_json['CLUB'] = club_names[ndx]
            single_club_json['WEBSITE'] = club_websites[ndx]
            
            all_club_json.append(single_club_json)

        final_club_json = {}
        final_club_json['clubs'] = all_club_json
        with open('department_resources/club.json', 'w') as fp:
            json.dump(final_club_json, fp, sort_keys=True, indent=4)

    def scrape_and_dump_student_count(self):

        myRequest = requests.get("https://csc.calpoly.edu/about/")
        soup = BeautifulSoup(myRequest.text, "html.parser")
        table = soup.find(attrs={"id": "mainLeftFull"})

        
        count = table.find_all('li')
        student_count_json = {}

        UNDERGRAD_COUNT = 0
        CS_SE_COUNT = [int(s) for s in (count[0].get_text()).split() if s.isdigit()]
        CPE_COUNT = [int(s) for s in (count[1].get_text()).split() if s.isdigit()]
        GRAD_COUNT = [int(s) for s in (count[2].get_text()).split() if s.isdigit()]
        for count in CS_SE_COUNT:
            UNDERGRAD_COUNT += count
        for count in CPE_COUNT:
            UNDERGRAD_COUNT += count
        GRAD_COUNT = count

        student_count_json['UNDERGRAD_COUNT'] = str(UNDERGRAD_COUNT)
        student_count_json['GRAD_COUNT'] = str(GRAD_COUNT)
        student_count_json['STUDENT_COUNT'] = str(UNDERGRAD_COUNT + GRAD_COUNT)
        with open('department_resources/student_count.json', 'w') as fp:
            json.dump(student_count_json, fp, sort_keys=True, indent=4)


    def load_jsons(self):
        dicts = []
        for path in self.JSON_PATHS:
            with open(path, 'r') as fp:
                dicts.append(json.load(fp))
        self.data = self.merge_dicts(dicts)


