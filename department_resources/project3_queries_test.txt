# CPE 466 KDD (Dr. Khosmood) Spring 2016
# This file contains all potential queries for the Department module
# Key: 1=clubs, 2=department, 3=dispatcher, 4=instructors, 5=general, 6=majors/curricula
2 What is the department web site? 0 | https://csc.calpoly.edu/ | The department's web site is: https://csc.calpoly.edu/
