# CPE 466 KDD (Dr. Khosmood) Spring 2016
# This file contains all potential queries for the Department module
# Key: 1=clubs, 2=department, 3=dispatcher, 4=instructors, 5=general, 6=majors/curricula
2 Is there a website for the latest news on the Computer Science department? 0 | https://csc.calpoly.edu/news/ | Yes, here it is: https://csc.calpoly.edu/news/ 
2 What is the Computer Science department news website? 0 | https://csc.calpoly.edu/news/ | Here it is: https://csc.calpoly.edu/news/ 
2 Where can I find more news on the Computer Science department? 0 | https://csc.calpoly.edu/news/ | You can find it here: https://csc.calpoly.edu/news/ 
2 What are the latest news on the Computer Science department? 0 | https://csc.calpoly.edu/news/ | Here are the latest news: https://csc.calpoly.edu/news/ 
2 What are the latest news and publications of the Computer Science department? 0 | https://csc.calpoly.edu/news/ | Here are the latest news and publications: https://csc.calpoly.edu/news/ 
2 What is the most recent news of the Computer Science department? 0 | https://csc.calpoly.edu/news/ | Here is the latest news: https://csc.calpoly.edu/news/ 
2 Is there a news archive for the Computer Science Department? 0 | https://csc.calpoly.edu/news/ | Yes, here it is: https://csc.calpoly.edu/news/ 
2 What are some of the recents news on the Computer Science department? 0 | https://csc.calpoly.edu/news/ | Here are some of the recent news: https://csc.calpoly.edu/news/ 
2 Where can I find more information on the news of the Computer Science department? 0 | https://csc.calpoly.edu/news/ | You can find it here: https://csc.calpoly.edu/news/
2 How far back does the news archive of the Computer Science Department go? 0 | https://csc.calpoly.edu/news/ | It goes all the way back to [YEAR]
2 How recent is the news archive of the Computer Science Department? 0 | https://csc.calpoly.edu/news/ | It is up to date, with the most recent news belonging to [YEAR]
2 How is the news archive of the Computer Science Department organized? 0 | https://csc.calpoly.edu/news/ | It is organized in one year intervals.
2 What happened during this [YEAR/MONTH]? 0 | https://csc.calpoly.edu/news/[YEAR/MONTH] | The following happened: https://csc.calpoly.edu/news/[YEAR/MONTH]
2 Is there any news in the month of [MONTH] in [YEAR]? 0 | https://csc.calpoly.edu/news/[YEAR/MONTH] | You can find possible news here: https://csc.calpoly.edu/news/[MONTH]
2 How many news articles did the Computer Science Department record in [MONTH]? 0 | https://csc.calpoly.edu/news/[YEAR/MONTH] | Cal Poly archives recorded [NUMBER] articles.
2 Is there any news on [DATE]? 0 | https://csc.calpoly.edu/news/[YEAR/MONTH] | You can find possible news here: https://csc.calpoly.edu/news/[DATE]
2 Is there any news in [YEAR]? 0 | https://csc.calpoly.edu/news/[YEAR/MONTH] | You can find possible news here: https://csc.calpoly.edu/news/[YEAR]
2 Is there any news related to the Computer Science Department in [YEAR]? 1 | https://csc.calpoly.edu/news/ | You can check here: https://csc.calpoly.edu/news/
2 Is there any news related to [ARTICLE]? 1 | https://csc.calpoly.edu/news/[ARTICLE] | You can check here: https://csc.calpoly.edu/news/[ARTICLE]
2 Did this [ARTICLE] relating to the Computer Science Department ever happen? 1 | https://csc.calpoly.edu/news/[ARTICLE] | You can check here: https://csc.calpoly.edu/news/[ARTICLE]
2 When did [ARTICLE] happen? 1 | https://csc.calpoly.edu/news/[ARTICLE] | It happened on [DATE].
2 What date did [ARTICLE] happen on? 1 | https://csc.calpoly.edu/news/[ARTICLE] | It happened on [DATE].
2 Is there an article with the title [ARTICLE]? 1 | https://csc.calpoly.edu/news/[ARTICLE] | [YES/NO]
2 What month did [ARTICLE] happen on? 1 | https://csc.calpoly.edu/news/[ARTICLE] | It happened in [MONTH].
2 What year did [ARTICLE] happen on? 1 | https://csc.calpoly.edu/news/[ARTICLE] | It happened in [YEAR].
2 Are there clubs in the Computer Science Department? 0 | https://csc.calpoly.edu/clubs/ | Yes, you can find more information here: https://csc.calpoly.edu/clubs/ 
2 Are there Computer Science clubs? 0 | https://csc.calpoly.edu/clubs/ | Yes, you can find more information here: https://csc.calpoly.edu/clubs/ 
2 How many clubs are in the Computer Science Department? 0 | https://csc.calpoly.edu/clubs/ | There are [NUMBER] clubs.
2 What are some clubs associated with the Computer Science Department? 0 | https://csc.calpoly.edu/clubs/ | Here are some clubs: [CLUBS].
2 What clubs are in the Computer Science Department? 0 | https://csc.calpoly.edu/clubs/ | Here are some clubs: [CLUBS].
2 Is [CLUB] part of the Computer Science Department? 0 | https://csc.calpoly.edu/clubs/ | [YES/NO].
2 Are there any clubs with no websites? 0 | https://csc.calpoly.edu/clubs/ | No, check out the websites here: https://csc.calpoly.edu/clubs/
2 Does every club in the Computer Science Department have a website? 0 | https://csc.calpoly.edu/clubs/ | Yes, check out the websites here: https://csc.calpoly.edu/clubs/
2 Where can I find more information about [CLUB]? 0 | https://csc.calpoly.edu/clubs/ | You can find more information here: [WEBSITE]
2 What is [CLUB]'s website? 0 | https://csc.calpoly.edu/clubs/ | [WEBSITE] is [CLUB]'s website.
2 What club is [WEBSITE] associated with? 0 | https://csc.calpoly.edu/clubs/ | That is the website of the [CLUB]
2 Does [CLUB] have a website? 0 | https://csc.calpoly.edu/clubs/ | Yes, here is the website: [WEBSITE]
2 What does the [CLUB] entail? 0 | https://csc.calpoly.edu/clubs/ | You can find more information on their website. [WEBSITE]
2 What Computer Science clubs are there at Cal Poly? 0 | https://csc.calpoly.edu/clubs/ | Here are the computer science clubs: [CLUBS]