# CPE 466 KDD (Dr. Khosmood) Spring 2016
# This file contains all potential events queries for the Department module
# Key: 1=clubs, 2=department, 3=dispatcher, 4=instructors, 5=general, 6=majors/curricula
2 Is there a website for the latest events of the Computer Science department? 0 |  https://csc.calpoly.edu/calendar/ | Yes, here it is: https://csc.calpoly.edu/calendar/
2 Is there a website for the latest Computer Science department events? 0 |  https://csc.calpoly.edu/calendar/ | Yes, here it is: https://csc.calpoly.edu/calendar/
2 Is there a website for the latest Computer Science events? 0 |  https://csc.calpoly.edu/calendar/ | Yes, here it is: https://csc.calpoly.edu/calendar/
2 Is there a website for Computer Science department events? 0 |  https://csc.calpoly.edu/calendar/ | Yes, here it is: https://csc.calpoly.edu/calendar/
2 Is there a website for Computer Science events? 0 |  https://csc.calpoly.edu/calendar/ | Yes, here it is: https://csc.calpoly.edu/calendar/
2 What day is [EVENT] on? 1 |  https://csc.calpoly.edu/calendar/ | [EVENT] is on [DATE]
2 When does [EVENT] start? 1 | https://csc.calpoly.edu/calendar/ | [EVENT] starts at [TIME]
2 What time does [EVENT] start? 1 |  https://csc.calpoly.edu/calendar/ | [EVENT] starts at [TIME]
2 Are there any upcoming Computer Science events? 1 | https://csc.calpoly.edu/ | [YES/NO]
2 Are there any upcoming events? 1 | https://csc.calpoly.edu/ | [YES/NO]
2 Are there any Computer Science department events? 1 | https://csc.calpoly.edu/ | [YES/NO]
2 What are the upcoming  Computer Science events? 1 | https://csc.calpoly.edu/ | The upcoming events are: [EVENT_UPCOMING_LIST]
2 What are the upcoming events? 1 | https://csc.calpoly.edu/ | The upcoming events are: [EVENT_UPCOMING_LIST]
2 How many upcoming events Computer Science events are there? 1 |  https://csc.calpoly.edu/calendar/ | There are [EVENT_UPCOMING_LIST_LEN] upcoming events
2 How many upcoming events are there? 1 |  https://csc.calpoly.edu/calendar/ | There are [EVENT_UPCOMING_LIST_LEN] upcoming events
2 What events are today? 1 |  https://csc.calpoly.edu/calendar/ | The events happening today are: [EVENT_CURDAY_LIST]
2 What events are on [DATE]? 1 | https://csc.calpoly.edu/calendar/ | The events happening on [DATE] are: [EVENT_DATE_LIST]
2 What events were on [DATE]? 1 | https://csc.calpoly.edu/calendar/ | The events that happened on [DATE] are: [EVENT_DATE_LIST]
2 What events are this year? 1 | https://csc.calpoly.edu/calendar/ | The events happening this year are: [EVENT_CURYEAR_LIST]
2 What events were last week? 1 | https://csc.calpoly.edu/calendar/ | The events that happened last week were: [EVENT_LAST_WEEK_LIST]
2 What events were last month? 1 | https://csc.calpoly.edu/calendar/ | The events that happened last month were: [EVENT_LAST_MONTH_LIST]
2 Can I subscribe to a service to hear of upcoming Computer Science department events? 0 | https://csc.calpoly.edu/calendar/ | You can subscribe to the calender via ICS file which can be downloaded from this webpage: https://csc.calpoly.edu/calendar/
2 Can I subscribe to a service to hear of upcoming Computer Science events? 0 | https://csc.calpoly.edu/calendar/ | You can subscribe to the calender via ICS file which can be downloaded from this webpage: https://csc.calpoly.edu/calendar/
2 How can I be notified of upcoming Computer Science events? 0 | https://csc.calpoly.edu/calendar/ | You can subscribe to the calender via ICS file which can be downloaded from this webpage: https://csc.calpoly.edu/calendar/
2 How can I be informed of upcoming Computer Science events? 0 | https://csc.calpoly.edu/calendar/ | You can subscribe to the calender via ICS file which can be downloaded from this webpage: https://csc.calpoly.edu/calendar/