import unittest

from department_resources.events import DepartmentEvents


class EventsTests(unittest.TestCase):
    def test_init(self):
        g = DepartmentEvents()

    def test_get_response(self):
        g = DepartmentEvents()
        r = g.get_response('Is there a website for the latest Computer Science department events?')
        self.assertEqual(r[0], 1)
        self.assertEqual(r[1], DepartmentEvents.SIG_NORMAL)
        self.assertEqual(r[2], 'Yes, here it is: https://csc.calpoly.edu/calendar/')
        
        r = g.get_response('DKFSDKJFSDFJKL')
        self.assertEqual(r[0], 0)
        self.assertEqual(r[1], DepartmentEvents.SIG_UNKNOWN)
        self.assertEqual(r[2], 'I couldn\'t understand your question. Sorry!')

if __name__ == '__main__':
    unittest.main()
