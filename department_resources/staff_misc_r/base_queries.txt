# CPE 466 KDD (Dr. Khosmood) Spring 2016
# This file contains all potential queries for the Department module
# Key: 1=clubs, 2=department, 3=dispatcher, 4=instructors, 5=general, 6=majors/curricula
2 How many professors are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [PROF_COUNT] faculty members in our department.
2 How many assistant professors are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [ASST_PROF_COUNT] faculty members in our department.
2 How many associate professors are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [ASSOC_PROF_COUNT] faculty members in our department.
2 How many lecturers are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [LEC_COUNT] faculty members in our department.
2 How many emeriti are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [EMERITI_COUNT] faculty members in our department.
2 How many active emeriti are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [EMERITI_COUNT] faculty members in our department.
2 How many staff are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are [STAFF_COUNT] staff members in our department.
2 What is the office for [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The office for [NAME] is located in Building [BDG_NUM] in Room [ROOM_NUM].
2 Where is the office for [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The office for [NAME] is located in Building [BDG_NUM] in Room [ROOM_NUM].
2 Tell me the office for [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The office for [NAME] is located in Building [BDG_NUM] in Room [ROOM_NUM].
2 What is the email for [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The email for [NAME] is [EMAIL].
2 Tell me the email for [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The email for [NAME] is [EMAIL].
2 What is the position of [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The position of [NAME] is [POSITION].
2 Tell me the position of [NAME]? 1 | https://csc.calpoly.edu/faculty/ | The position of [NAME] is [POSITION].
2 Who are the professors in the department? 0 | https://csc.calpoly.edu/faculty/ | {[NAME}] are professors in our department.
2 Who are the assistant professors in the department? 0 | https://csc.calpoly.edu/faculty/ | {[NAME}] are assistant professors in our department.
2 Who are the associate professors in the department? 0 | https://csc.calpoly.edu/faculty/ | {[NAME}] are associate professors in our department.
2 Who are the lecturers in the department? 0 | https://csc.calpoly.edu/faculty/ | {[NAME]} are lecturers in our department.
2 Who are the active emeriti in the department? 0 | https://csc.calpoly.edu/faculty/ | {[NAME}] are active emeriti in our department.
2 Who are the staff in the department? 0 | https://csc.calpoly.edu/faculty/ | {[NAME}] are staff in our department.
2 What do my contributions support? 0 | https://csc.calpoly.edu/giving/ | {[INITIAVES}] are the current strategic initiatives supported by your contributions.
2 What do my donations support? 0 | https://csc.calpoly.edu/giving/ | {[INITIAVES}] are the current strategic initiatives supported by your donations.
2 What is the IAB? 0 | https://csc.calpoly.edu/iab/ | The IAB is the Industrial Advisory Board for our department.
2 Is [NAME] a member of the IAB? 1 | https://csc.calpoly.edu/iab/ | [YES/NO]
2 Who are the members on the IAB? 0 | https://csc.calpoly.edu/iab/ | { [NAME] from [COMPANY] } ... [is | are] on our department's Industrial Advisory Board.
2 What company does [NAME] work for? 1 | https://csc.calpoly.edu/iab/ | [NAME] from [COMPANY]  is on the Industrial Advisory Board.
2 What members of the IAB work for [COMPANY]? 1 | https://csc.calpoly.edu/iab/ | {[NAME]} from [COMPANY] [is | are] on the Industrial Advisory Board.
2 Where do I find Academic Advising forms? 0 | https://csc.calpoly.edu/forms/ | You can find Academic Advising forms at [forms_located_in_academic advising].
2 Where do I find Registrar forms? 0 | https://csc.calpoly.edu/forms/ | You can find Registrar forms at [forms_located_in_the_office_of_the_registrar].
2 Where do I find Lab Equipment Sign-out forms? 0 | https://csc.calpoly.edu/forms/ | You can find Lab Equipment Sign-out forms at [lab_equipment_sign-out].
2 Where do I find Prerequisite Waiver Request forms? 0 | https://csc.calpoly.edu/forms/ | You can find Prerequisite Waiver Request forms at [prerequisite_waiver_request].
2 Where do I find Senior Project Requirement forms? 0 | https://csc.calpoly.edu/forms/ | You can find Senior Project Requirement forms at [senior_project_requirement].
2 Where do I find Senior Project Pre-enrollment Proposal forms? 0 | https://csc.calpoly.edu/forms/ | You can find Senior Project Pre-enrollment Proposal forms at [senior_project_pre-enrollment_proposal].
2 Where do I find Supervisory Course Enrollment forms? 0 | https://csc.calpoly.edu/forms/ | You can find Supervisory Course Enrollment forms at [supervisory_course enrollment].
2 Where do I find Blended BS + MS Application forms? 0 | https://csc.calpoly.edu/forms/ | You can find Blended BS + MS Application forms at [blended_bs_+_ms_application].
2 Where do I find Blended BS + MS Letter of Recommendation forms? 0 | https://csc.calpoly.edu/forms/ | You can find Blended BS + MS Letter of Recommendation forms at [blended_bs_+_ms_letter_of_recommendation].
2 Where do I find Postbaccalaureate Change of Objective forms? 0 | https://csc.calpoly.edu/forms/ | You can find Postbaccalaureate Change of Objective forms at [postbaccalaureate_change_of_objective].
2 Where do I find JD Moore Doctoral Fellowship Application forms? 0 | https://csc.calpoly.edu/forms/ | You can find JD Moore Doctoral Fellowship Application forms at [jl_moore_doctoral_fellowship_application].
2 Where do I find Graduate Request for Graduate Evaluation forms? 0 | https://csc.calpoly.edu/forms/ | You can find Graduate Request for Graduate Evaluation forms at [graduate_request_for_graduate_evaluation].
2 Where do I find Advancement to Candidacy forms? 0 | https://csc.calpoly.edu/forms/ | You can find Advancement to Candidacy forms at [advancement_to_candidacy].
2 Where do I find Degree Completion Checklist forms? 0 | https://csc.calpoly.edu/forms/ | You can find Degree Completion Checklist forms at [degree_completion_checklist].
2 Where do I find Final Formal Study Plan forms? 0 | https://csc.calpoly.edu/forms/ | You can find Final Formal Study Plan forms at [final_formal_study_plan].
2 Where do I find Master's Degree Approval forms? 0 | https://csc.calpoly.edu/forms/ | You can find Master's Degree Approval forms at [masters_thesis_project_exam_approval].
2 Where do I find Working Formal Study Plan forms? 0 | https://csc.calpoly.edu/forms/ | You can find Working Formal Study Plan forms at [working_formal_study_plan].