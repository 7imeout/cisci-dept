import unittest

from department_resources.buildings import DepartmentBuildings


class BuildingsTests(unittest.TestCase):
    def test_init(self):
        g = DepartmentBuildings()

    def test_get_response(self):
        g = DepartmentBuildings()
        r = g.get_response('Where can I find the [BUILDING_NAME] map?')
        self.assertEqual(r[0], 1)
        self.assertEqual(r[1], DepartmentBuildings.SIG_NORMAL)
        self.assertEqual(r[2], '[BUILDING_NAME]’s map can be found at https://afd.calpoly.edu/facilities/maps_floorplans.asp')
        
        r = g.get_response('DKFSDKJFSDFJKL')
        self.assertEqual(r[0], 0)
        self.assertEqual(r[1], DepartmentBuildings.SIG_UNKNOWN)
        self.assertEqual(r[2], 'I couldn\'t understand your question. Sorry!')

if __name__ == '__main__':
    unittest.main()
