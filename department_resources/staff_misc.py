import sys, random, json, requests, re
from bs4 import BeautifulSoup

from department_resources.meta import DepartmentMeta


class DepartmentStaffMisc(DepartmentMeta):
    dep = "department_resources/forms.json"
    JSON_PATHS = []
    DEBUG = False

    def __init__(self, queries_path):
        self.JSON_PATHS.append(self.dep)
        super().__init__(queries_path)
        self.data = {}

    def get_form_links(self):
        formsURL = "https://csc.calpoly.edu/forms/"
        myRequest = requests.get(formsURL)
        soup = BeautifulSoup(myRequest.text, "html.parser")
        boccat = soup.find(id="mainLeftFull")
        form_links = {}
        pBreaks = boccat.find_all('p')
        for link in pBreaks:
            temp = link.find(text=True).lower()
            temp = temp.replace(" ", "_")
            temp = temp.replace("/", "_")
            temp = temp.replace("'", "")
            form_links[temp] = link.find('a')['href']
        with open('department_resources/forms.json', 'w') as fp:
            json.dump(form_links, fp, sort_keys=True, indent=4)

    def get_contact_stuff(self):
        contactURL = "https://csc.calpoly.edu/contact/"
        myRequest = requests.get(contactURL)
        soup = BeautifulSoup(myRequest.text, "html.parser")
        boccat = soup.find(id="mainLeftFull")
        stuff = {}
        pBreaks = boccat.find_all('br')
        for line in pBreaks:
            if line != None:
                lineText = line.find(text=True)
                if lineText != None:
                    lineText = lineText.lower()
                    if 'building' in lineText:
                        extractedLine = re.search(r'building (\d+), room (\d+)', lineText)
                        stuff['bdg_num'] = extractedLine.group(1)
                        stuff['room_num'] = extractedLine.group(2)
                        stuff['cs_office'] = extractedLine.group(2)
                    elif 'phone' in lineText:
                        extractedLine = re.search(r'phone: (\d\d\d-\d\d\d-\d\d\d\d)', lineText)
                        stuff['phone_num'] = extractedLine.group(1)
                    elif 'fax' in lineText:
                        extractedLine = re.search(r'fax: (\d\d\d-\d\d\d-\d\d\d\d)', lineText)
                        stuff['fax_num'] = extractedLine.group(1)
                    elif '@' in lineText:
                        stuff['email'] = lineText
                    elif 'monday' in lineText:
                        extractedLine = re.search(r'(\w+) – (\w+)', lineText)
                        stuff['weekday_department_open_start'] = extractedLine.group(1)
                        stuff['weekday_department_open_end'] = extractedLine.group(2)
                    elif 'pm' in lineText:
                        extractedLine = re.search(r'(\d:\d\dam)-(\d\d:\d\dpm); (\d:\d\dpm)-(\d:\d\dpm)', lineText)
                        stuff['time_department_open'] = extractedLine.group(1)
                        stuff['day_start'] = extractedLine.group(1)
                        stuff['time_department_close'] = extractedLine.group(4)
                        stuff['day_stop'] = extractedLine.group(4)
                        stuff['time_department_lunch'] = extractedLine.group(2)
                        stuff['time_department_end_lunch'] = extractedLine.group(3)
                        stuff['time'] = lineText
                    stuff['mailing_address'] = 'Computer Science Department, Building 14, Room 254, California Polytechnic State University, San Luis Obispo, Ca 93407-0354'
        with open('department_resources/contact.json', 'w') as fp:
            json.dump(stuff, fp, sort_keys=True, indent=4)

    def update(self):
        """
        Updates the source information used to answer questions (i.e. department chair's name)
        :return: True if successful, False otherwise (?)
        """
        try:
            self.get_form_links()
            self.get_contact_stuff()
            super().update()
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror))
            return False
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return True

    def fill_variables(self):
        for question_key in self.questions.keys():
            response = self.questions[question_key][self.RESPONSE_INDEX]
            vars = re.findall(self.VARIABLE_REGEX, response)
            for var in vars:
                var_name = var[1:-1]
                if var_name in self.data.keys():
                    response = re.sub('\[' + var_name + '\]', self.data[var_name], response)
                elif self.DEBUG:
                    print("variable substitution failed for: " + var + ' in \"' + response + '\"', file=sys.stderr)
                self.questions[question_key][self.RESPONSE_INDEX] = response

    def load_json(self):
        dicts = []
        for path in self.JSON_PATHS:
            with open(path, 'r') as fp:
                dicts.append(json.load(fp))
        self.data = self.merge_dicts(dicts)