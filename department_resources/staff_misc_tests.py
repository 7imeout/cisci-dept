import unittest

from department_resources.staff_misc import DepartmentStaffMisc


class StaffMiscTests(unittest.TestCase):
    def test_init(self):
        g = DepartmentStaffMisc()

    def test_get_response(self):
        g = DepartmentStaffMisc()
        r = g.get_response('What do my donations support?')
        self.assertEqual(r[0], 1)
        self.assertEqual(r[1], DepartmentStaffMisc.SIG_NORMAL)
        self.assertEqual(r[2], '{[INITIAVES}] are the current strategic initiatives supported by your donations.')
        r = g.get_response('DKFSDKJFSDFJKL')
        self.assertEqual(r[0], 0)
        self.assertEqual(r[1], DepartmentStaffMisc.SIG_UNKNOWN)
        self.assertEqual(r[2], 'I couldn\'t understand your question. Sorry!')

if __name__ == '__main__':
    unittest.main()
