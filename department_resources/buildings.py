import sys, random, re, requests, json
from bs4 import BeautifulSoup

from department_resources.meta import DepartmentMeta


class DepartmentBuildings(DepartmentMeta):

    DEBUG = False
    JSON_PATHS = ("department_resources/contact.json", "department_resources/buildings.json")

    def __init__(self, queries_path):
        super().__init__(queries_path)
        super().update()

    def fill_variables(self):
        for question_key in self.questions.keys():
            response = self.questions[question_key][self.RESPONSE_INDEX]
            a_variables = re.findall(self.VARIABLE_REGEX, response)

            for var in a_variables:
                var_name = var[1:-1]
                if var_name in self.data.keys():
                    replace = self.data[var_name]
                    if (isinstance(replace, list)):
                        replace = ', '.join(replace)
                    response = re.sub('\[' + var_name + '\]', replace, response)
                else:
                    if self.DEBUG:
                        print("variable substitution failed for: " + var + ' in \"' +  response + '\"', file=sys.stderr)
                    self.questions[question_key][self.FLAG_INDEX] = 2 #Question definately needs runtime processing
            self.questions[question_key][self.RESPONSE_INDEX] = response

    #Override of meta
    def get_best_tf_idf(self, query, history=[]):
        query_words = query.split()
        best_question, best_tf_idf = self.get_question_scores(query_words)[0]
        best_response = self.questions[best_question][self.RESPONSE_INDEX]

        #Processing if the question contains variables used by the answer
        question = best_question
        response = best_response
        q_variables = re.findall(self.VARIABLE_REGEX, question)
        a_variables = re.findall(self.VARIABLE_REGEX, response)
        a_vars = [var[1:-1].lower() for var in a_variables]
        actual = ''

        for var in q_variables:
            var_name = var[1:-1]
            var_name_br = '\[' + var_name + '\]'
            match = re.search(r'(.*)' +var_name_br +r'(.*)', question)
            before = match.group(1)
            after = match.group(2)[:-1] #Remove any possible '?'
            try:
                match = re.search(r'' +before +r'(.*)' +after, query)
                actual = match.group(1)
                if var_name in a_vars:
                    response = re.sub(var_name_br.lower(), actual, response)
                    response = re.sub(var_name_br.upper(), actual, response)
            except:
                pass #failiure
        
        #Questions that require runtime processing to answer
        a_variables = re.findall(self.VARIABLE_REGEX, response)
        if (a_variables):
            replacements = []
            if (question == 'What is the building number of [BUILDING_NAME]?'.lower()) \
                    and 'BUILDING_NAME_TO_NUM_DICT' in self.data:
                name_to_num = self.data['BUILDING_NAME_TO_NUM_DICT']
                if (actual in name_to_num):
                    replacements.append(name_to_num[actual])

            elif (question == 'What is the name of building [BUILDING_NUM]?'.lower()) \
                    and 'BUILDING_NUM_TO_NAME_DICT' in self.data:
                num_to_name = self.data['BUILDING_NUM_TO_NAME_DICT']
                if (actual in num_to_name):
                    replacements.append(num_to_name[actual])

            elif (question == 'What Computer Science classrooms does building [BUILDING_NUM] contain?'.lower()) \
                    and 'CS_BUILDING_DICT' in self.data:
                cs_bd_dict = self.data['CS_BUILDING_DICT']
                if (actual in cs_bd_dict.keys()):
                    replacements.append(', '.join(cs_bd_dict[actual]))
                else:
                    response = 'Building ' +actual +' contains no Computer Science classrooms'

            elif (question == 'How many Computer Science classrooms does building [BUILDING_NUM] contain?'.lower()) \
                    and 'CS_BUILDING_DICT' in self.data:
                cs_bd_dict = self.data['CS_BUILDING_DICT']
                if (actual in cs_bd_dict.keys()):
                    replacements.append(str(len(cs_bd_dict[actual])))
                else:
                    response = 'Building ' +actual +' contains no Computer Science classrooms'

            elif (question == 'Does [BUILDING_NAME] contain Computer Science classrooms?'.lower())\
                    and 'BUILDING_NAME_TO_NUM_DICT' in self.data:
                name_to_num = self.data['BUILDING_NAME_TO_NUM_DICT']
                if (actual in name_to_num):
                    num = name_to_num[actual]
                    if (num in self.data['CS_BUILDING_LIST']):
                        replacements.append('YES')
                    else:
                        replacements.append('NO')

            elif (question == 'Does building [BUILDING_NUM] contain Computer Science classrooms?'.lower())\
                    and 'CS_BUILDING_DICT' in self.data:
                cs_bd_dict = self.data['CS_BUILDING_DICT']
                if (actual in cs_bd_dict.keys()):
                    replacements.append('YES')
                else:
                    replacements.append('NO')

            elif (question == 'What building is [CS_CLASSROOM] in?'.lower())\
                    and 'CS_BUILDING_DICT' in self.data and 'BUILDING_NUM_TO_NAME_DICT' in self.data:
                cs_bd_dict = self.data['CS_BUILDING_DICT']
                num_to_name = self.data['BUILDING_NUM_TO_NAME_DICT']
                buildNum = None
                
                for build in cs_bd_dict:
                    if (actual in cs_bd_dict[build]):
                        buildNum = build
                        break
                if (buildNum):
                    replacements.append(num_to_name[buildNum])
                else:
                    response = 'That classroom is not in the Computer Science department'

            #The following questions are not implemented because they are:
            #overly specific and the information is from the csc wiki so it will not be updated
            #In any case I implemented these three questions (plus one more) for my lab 6 submission anyway
            elif (question == 'What Computer Science classrooms are available for [QUARTER] [YEAR]?'.lower()):
                pass
            elif (question == 'How many Computer Science classrooms are available for [QUARTER] [YEAR]?'.lower()):
                pass
            elif (question == 'What Computer Science classrooms close at [HOUR] [AM/PM]?'.lower()):
                pass

            if (replacements):
                for i in range(len(a_variables)):
                    var = a_variables[i]
                    replace = replacements[i]
                    var_name_br = '\[' + var[1:-1] + '\]'
                    try:
                        response = re.sub(var_name_br.lower(), replace, response)
                        response = re.sub(var_name_br.upper(), replace, response)
                    except:
                        pass #failiure

        best_response = response
        return best_response, best_tf_idf

    def update(self):
        try:
            self.scrapeDump()
            super().update()
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror))
            return False
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        return True

    def scrapeDump(self):
        quarterInfos = self.scrapeLastClassInfo()
        labs = self.scrapeLabList()
        bd_num_to_name, bd_name_to_num = self.scrapeFloorPlans()

        rooms = []
        for qtr in quarterInfos:
            rooms += list(quarterInfos[qtr].keys())
        rooms = list(set(rooms)) #Remove duplicates
        intersection = list(set(rooms).intersection(labs))

        buildings = []
        buildingsDict = {}
        for room in rooms:
            match = re.search(r'(\d+)-', room)
            bd = match.group(1)
            buildings.append(bd)
            if (bd not in buildingsDict):
                buildingsDict[bd] = []
            buildingsDict[bd].append(room)
                
        buildings = list(set(buildings)) #Remove duplicates
        
        building_dict = {}
        building_dict['CS_BUILDING_LIST'] = buildings
        building_dict['CS_BUILDING_DICT'] = buildingsDict
        building_dict['CS_BUILDING_LIST_LEN'] = str(len(buildings))
        building_dict['CS_CLASSROOMS_LIST'] = rooms
        building_dict['CS_CLASSROOMS_LIST_LEN'] = str(len(rooms))
        building_dict['CS_LABS_LIST'] = labs
        building_dict['CS_LABS_LIST_LEN'] = str(len(labs))
        building_dict['CS_CLASSROOM_LABS_INTERSECTION_LIST'] = intersection
        building_dict['CS_CLASSROOM_LABS_INTERSECTION_LIST_LEN'] = str(len(intersection))
        building_dict['BUILDING_NUM_TO_NAME_DICT'] = bd_num_to_name
        building_dict['BUILDING_NAME_TO_NUM_DICT'] = bd_name_to_num
        
        with open('department_resources/buildings.json', 'w') as fp:
            json.dump(building_dict, fp, sort_keys=True, indent=4)

    def load_json(self):
        dicts = []
        for path in self.JSON_PATHS:
            with open(path, 'r') as fp:
                dicts.append(json.load(fp))
        self.data = self.merge_dicts(dicts)

    def scrapeLastClassInfo(self):
        myRequest = requests.get("http://frank.ored.calpoly.edu/LastClassInfo")
        soup = BeautifulSoup(myRequest.text,"html.parser")

        wiki = soup.find(attrs={"class": "wiki", "id": "content"})
        h1h2 = wiki.find_all(['h1','h2'])
        tables = wiki.find_all('table')

        quarters = ['fall', 'winter', 'spring', 'summer']
        scrapedQuarters = []
        for head in h1h2:
            quart = head.string.split()[0].lower()
            if (quart in quarters):
                scrapedQuarters.append(head.string)

        quarterInfos = {}
        for i in range(len(scrapedQuarters)):
            quart = scrapedQuarters[i]
            table = tables[i]

            rows = table.find_all('tr')
            header = rows[0].find_all('td')
            cols = [item.string.strip().lower() for item in header]

            roomInfo = {}
            for i in range(1, len(rows)):
                row = rows[i]
                info = [item.string.strip() for item in row.find_all('td')]
                if ((info[1] == "PM" or info[1] == "NA") and (info[2] == "PM" or info[2] == "NA")):
                    roomInfo[info[0].lower()] = None
                else:
                    roomInfo[info[0].lower()] = {cols[i]: info[i].lower() for i in range(1,len(info))}
            quarterInfos[quart.lower()] = roomInfo

        return quarterInfos

    def scrapeLabList(self):
        myRequest = requests.get("http://frank.ored.calpoly.edu/LabList")
        soup = BeautifulSoup(myRequest.text,"html.parser")

        wiki = soup.find(attrs={"id": "content"})
        h2 = wiki.find_all(['h2'])
        labs = [header['id'] for header in h2]
        labRooms = [re.search(r'.*(\d\d-\d+\w*)', lab).group(1).lower() for lab in labs]

        return labRooms

    def scrapeFloorPlans(self):
        myRequest = requests.get("https://afd.calpoly.edu/facilities/maps_floorplans.asp")
        soup = BeautifulSoup(myRequest.text,"html.parser")
        table = soup.find('table')
        rows = table.find_all('tr')

        bd_num_to_name = {}
        bd_name_to_num = {}
        for i in range(1, len(rows)):
            row = rows[i]
            building = row.find('a').string
            match = re.search(r'(\d+\w*-*\w*)\S* (.*)', building)
            number = match.group(1)
            name = match.group(2)
            bd_num_to_name[number] = name
            bd_name_to_num[name.lower()] = number

        return bd_num_to_name, bd_name_to_num
