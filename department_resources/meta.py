""" Meta class for the sub-module classes of Department module.

This serves as a common interface for the sub-modules of Department module.

"""

import re, sys, math, operator
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer


class DepartmentMeta(object):
    """This is a meta class that serves as a common interface for the sub-module classes.

    When you write the sub-module classes (see general.py for an example),
    please extend this class and override the three methods that this
    does not have the implementation for.

    This interface closely mirrors how the top-level module, department.py
    is set up, and will operate very similarly. You may consider our top-
    level department.py as our own "sub-dispatcher" that will rely on your
    sub-module's interface compliance and functionality.

    Attributes:
        data (dint): Dictionary from JSONs (not populated by default)
        questions (dict): Dictionary with key: string -> value: list
            key (string) signifies the question string
            value (list) is [key, flag, url, answer]

    Static Constants:
        Indexes for the list: KEY_INDEX, FLAG_INDEX, URL_INDEX, RESPONSE_INDEX
        Signal strings: SIG_NORMAL, SIG_ERROR, SIG_QUESTION, SIG_UNKNOWN, SIG_END

    """

    META_DEBUG = False

    KEY_INDEX = 0
    FLAG_INDEX = 1
    URL_INDEX = 2
    RESPONSE_INDEX = 3

    SPLIT_LINE_QUESTION_INDEX = 0
    SPLIT_LINE_URL_INDEX = 1
    SPLIT_LINE_RESPONSE_INDEX = 2

    SIG_NORMAL = "Normal"
    SIG_ERROR = "Error"
    SIG_QUESTION = "Question"
    SIG_UNKNOWN = "Unknown"
    SIG_END = "End"

    VARIABLE_REGEX = r"(\[\w+\])"

    def __init__(self, queries_path):
        self.queries_path = queries_path
        self.split_lines = []

        self.data = {}  # data from JSONs
        self.questions = {}

        self.stemmed_questions = {}
        self.all_words = []
        self.vocab = set()

        try:
            self.process_base_queries(self.queries_path)
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror))
        except:
            print("Unexpected error while parsing base queries:", sys.exc_info()[0])
            raise


    def update(self):
        """
        Updates the source information used to answer questions (i.e. department chair's name)
        :return: True if successful, False otherwise (?)
        """
        try:
            self.load_json()
            self.fill_variables()
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror), file=sys.stderr)
            return False
        except:
            print("Unexpected error during updating:", sys.exc_info()[0], file=sys.stderr)
            return False
        return True

    def get_rating(self, query, history=[]):
        """
        Returns rating based on query and (if you wish) the history
        :param query: current query
        :param history: optional; list of the query / response history (?)
        :return: rating of confidence regarding our answer to the current query
        """
        rating = 1.0 if query in self.questions else 0.0
        if not query.lower() in self.questions:
            response_string, rating = self.get_best_guess(query, history)
        return rating

    def get_response(self, query, history=[]):
        """
        Returns the response to the given query.
        :param query: current query
        :param history: optional; list of the query / response history (?)
        :return: our best response for the query
        """
        self.get_best_tf_idf(query)

        rating = 0.0
        if query.lower() in self.questions:
            rating = 1.0
            response_string = self.questions[query.lower()][DepartmentMeta.RESPONSE_INDEX]
            signal = DepartmentMeta.SIG_NORMAL
        else:
            response_string = self.get_best_tf_idf(query, history)[0] # self.get_best_guess(query, history)
            rating = self.get_best_guess(query, history)[1]
            signal = self.SIG_NORMAL

        return [rating, signal, response_string]


    def load_json(self):
        """
        Override this method to load necessary information from the JSON files.
        This method will be called by meta's update method to refresh loaded data.
        """
        pass

    def fill_variables(self):
        """
        Once you're ready to process real data (meaning using questions that have
        variables), edit the file path corresponding to your sub-module in department.py
        to use base_queries.txt instead of canned_queries.txt, then override this method
        to do necessary filling in of the variables in the answers.
        """
        pass

    def get_best_guess(self, query, history=[]):
        query_words = frozenset(query.split())
        curr_max = -1
        curr_best = "I couldn\'t understand your question. Sorry!"
        curr_best_words = []
        for question in self.questions:
            question_words = frozenset(question.replace('?', '').split())
            match_size = len(query_words.intersection(question_words))
            if match_size > curr_max:
                curr_max = match_size
                curr_best = self.questions[question][DepartmentMeta.RESPONSE_INDEX]
                curr_best_words = question_words
        rating =  0.0 if len(curr_best_words) == 0 else curr_max / len(curr_best_words)
        return curr_best, rating

    def process_base_queries(self, file_path):
        file = open(file_path, 'r')
        lines = [line for line in file.readlines() if len(line) > 0 and line.lstrip()[0] != '#']

        self.split_lines = map(lambda l: l.split('|'), lines)
        self.fill_questions(lines)
        self.fill_vocab_and_stemmed(lines)

    def fill_questions(self, lines):
        self.questions = {}
        for line in self.split_lines:
            trimmed = line[self.SPLIT_LINE_QUESTION_INDEX].strip()
            q = trimmed[1:].strip()[:-1].rstrip()
            key = int(trimmed[0])
            url = line[self.SPLIT_LINE_URL_INDEX].strip()
            answer = line[self.SPLIT_LINE_RESPONSE_INDEX].strip()
            variables = re.findall(self.VARIABLE_REGEX, answer)
            if (not variables):
                flag = int(trimmed[-1])
            else:
                flag = 2 #Question might require runtime processing to answer
            self.questions[q.lower()] = [key, flag, url, answer]

    def fill_vocab_and_stemmed(self, lines):
        stemmer = SnowballStemmer('english', ignore_stopwords=True)
        if not self.questions:
            self.fill_questions(lines)
        self.vocab = set()
        for q in self.questions.keys():
            stemmed_question = set()
            for word in q.split():
                word = re.sub(r'\W', '', word).lower()
                stemmed_word = stemmer.stem(word)
                self.vocab.add(word)
                self.vocab.add(stemmed_word)
                self.all_words.append(word)
                self.all_words.append(stemmed_word)
                stemmed_question.add(stemmed_word)
            self.stemmed_questions[frozenset(stemmed_question)] = q

    def merge_dicts(self, dicts):
        """
        Given a list of dicts, shallow copy and merge into a new dict, precedence goes to key value pairs in latter dicts.
        http://stackoverflow.com/questions/38987/how-can-i-merge-two-python-dictionaries-in-a-single-expression
        :return: single merged dictionary
        """
        result = {}
        for dictionary in dicts:
            result.update(dictionary)
        return result

    def get_best_tf_idf(self, query, history=[]):
        query_words = query.split()
        best_question, best_tf_idf = self.get_question_scores(query_words)[0]
        best_response = self.questions[best_question][self.RESPONSE_INDEX]
        return best_response, best_tf_idf

    def get_doc_freqs(self, term):
        all_freqs = 0
        for q in self.questions.keys():
            all_freqs += 1 if term.lower() in q.replace('?', '').split() else 0
        return all_freqs

    def get_question_scores(self, terms):
        question_scores = {}
        for q in self.questions.keys():
            total = 0
            q_words = q.replace('?', '').split()
            for term in terms:
                doc = q_words
                tf = doc.count(term.lower())
                idf = math.log((len(self.questions) + 1) / (self.get_doc_freqs(term) + 1))
                total += tf * idf
            question_scores[q] = total
        sorted_docs = sorted(question_scores.items(), key=operator.itemgetter(1), reverse=True)
        if self.META_DEBUG:
            print(self.__class__.__name__)
            for entry in sorted_docs[:5]:
                print(entry[0], entry[1])
            print()
        return sorted_docs
