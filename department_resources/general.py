import sys, random, json, requests, re
from bs4 import BeautifulSoup

from department_resources.meta import DepartmentMeta


class DepartmentGeneral(DepartmentMeta):

    DEBUG = False

    ROLE_NDX = 0
    POSITION_NDX = ROLE_NDX + 1
    EMAIL_NDX = ROLE_NDX + 2
    OFFICE_NDX = ROLE_NDX + 3
    INDEXES = {'role': ROLE_NDX, 'position': POSITION_NDX, 'email': EMAIL_NDX, 'office': OFFICE_NDX}
    TARGET_URL = "https://csc.calpoly.edu/faculty/"

    JSON_PATHS = ("department_resources/contact.json",
                  "department_resources/faculty.json",
                  "department_resources/student_count.json")

    def __init__(self, queries_path):
        super().__init__(queries_path)
        super().update()

    def update(self):
        """
        Updates the source information used to answer questions (i.e. department chair's name)
        :return: True if successful, False otherwise (?)
        """
        try:
            self.scrape_and_dump_json()
            super().update()
        except IOError as err:
            print("I/O error({0}): {1}".format(err.errno, err.strerror))
            return False
        except:
            print("Unexpected error:", sys.exc_info()[0])
            return False
        return True

    def load_json(self):
        dicts = []
        for path in self.JSON_PATHS:
            with open(path, 'r') as fp:
                dicts.append(json.load(fp))
        self.data = self.merge_dicts(dicts)

    def fill_variables(self):
        for question_key in self.questions.keys():
            response = self.questions[question_key][self.RESPONSE_INDEX]
            vars = re.findall(self.VARIABLE_REGEX, response)
            for var in vars:
                var_name = var[1:-1]
                if var_name in self.data.keys():
                    response = re.sub('\[' + var_name + '\]', self.data[var_name], response)
                elif self.DEBUG:
                    print("variable substitution failed for: " + var + ' in \"' +  response + '\"', file=sys.stderr)
            self.questions[question_key][self.RESPONSE_INDEX] = response

    def scrape_and_dump_json(self):
        self.scrape_and_dump_faculty()

    def scrape_and_dump_faculty(self):
        myRequest = requests.get(self.TARGET_URL)
        soup = BeautifulSoup(myRequest.text, "html.parser")
        tables = soup.find_all(attrs={"class": "blankTable faculty"})
        roles = list(map(lambda h2: h2.string, soup.find_all('h2')))[1:]
        subHeaders = list(map(lambda h2: h2.string, soup.find_all('h3')))[1:]
        roles = (roles[0], subHeaders[0], subHeaders[1], roles[1])

        personnel = {}
        role_index = 0
        prof_count = 0
        faculty_count = 0
        lecturer_count = 0
        staff_count = 0

        for table in tables:
            firstRow = table.find('tr')
            columnHeaders = [element.string.lower() for element in firstRow.find_all('th')]
            for tr in table.find_all('tr'):
                full_name = 'N/A'
                is_chair = False
                name_key = None
                if tr.find('a'):
                    full_name = tr.find('a').string
                    name = full_name.lower().split()
                    name_key = name[0][0] + "_" + name[-1]
                    personnel[name_key] = [roles[role_index], 'N/A', 'N/A', '14-254 (Dept. Office)']
                if tr.find('span'):
                    personnel[name_key][self.INDEXES['position']] = tr.find('span').string
                if tr.find('strong'):
                    is_chair = True
                    personnel[name_key][self.INDEXES['position']] = tr.find('strong').string
                if tr.find_all('td'):
                    personnel[name_key][self.INDEXES[columnHeaders[1]]] = tr.find_all('td')[1].string.replace(
                        '(place an \'at\' sign here)', '@')
                    personnel[name_key][self.INDEXES[columnHeaders[2]]] = tr.find_all('td')[2].string
                if is_chair:
                    personnel['dept_chair_name'] = "Dr. " + full_name
                    personnel['dept_chair_email'] = personnel[name_key][self.INDEXES['email']]
                if name_key and \
                    'admin' in personnel[name_key][self.INDEXES['position']].lower() and \
                    'coord' in personnel[name_key][self.INDEXES['position']].lower():
                    personnel['dept_admin_coord_pos'] = personnel[name_key][self.INDEXES['position']]
                    personnel['dept_admin_coord_name'] = full_name
                    personnel['dept_admin_coord_email'] = personnel[name_key][self.INDEXES['email']]
                if name_key and ('professor' in personnel[name_key][self.INDEXES['position']].lower()
                                 or 'chair' in personnel[name_key][self.INDEXES['position']].lower()):
                    prof_count += 1
                if name_key and 'faculty' in personnel[name_key][self.INDEXES['role']].lower():
                    faculty_count += 1
                if name_key and 'lecture' in personnel[name_key][self.INDEXES['role']].lower():
                    lecturer_count += 1
                if name_key and 'staff' in personnel[name_key][self.INDEXES['role']].lower():
                    staff_count += 1
            role_index += 1

        personnel['teacher_count'] = str(faculty_count + lecturer_count)
        personnel['prof_count'] = str(prof_count)
        personnel['faculty_count'] = str(faculty_count)
        personnel['lecturer_count'] = str(lecturer_count)
        personnel['staff_count'] = str(staff_count)

        with open('department_resources/faculty.json', 'w') as fp:
            json.dump(personnel, fp, sort_keys=True, indent=4)