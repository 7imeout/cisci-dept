# CPE 466 KDD (Dr. Khosmood) Spring 2016
# This file contains all potential queries for the Department module
# Key: 1=clubs, 2=department, 3=dispatcher, 4=instructors, 5=general, 6=majors/curricula
2 What is the department web site? 0 | https://csc.calpoly.edu/ | The department's web site is: https://csc.calpoly.edu/
2 What is the computer science department web site? 0 | https://csc.calpoly.edu/ | The department's web site is: https://csc.calpoly.edu/
2 What is the CSC department web site? 0 | https://csc.calpoly.edu/ | The department's web site is: https://csc.calpoly.edu/
2 What is the software engineering department web site? 0 | https://csc.calpoly.edu/ | Cal Poly doesn't have an SE department, but the CSC (Computer Science) department's web site is: https://csc.calpoly.edu/
2 What is the SE department web site? 0 | https://csc.calpoly.edu/ | Cal Poly doesn't have an SE department, but the CSC (Computer Science) department's web site is: https://csc.calpoly.edu/
2 Is there a department site? 0 | https://csc.calpoly.edu/ | Yes, the department's web site is: https://csc.calpoly.edu/
2 Is there a web site I can visit? 0 | https://csc.calpoly.edu/ | Yes, the department's web site is: https://csc.calpoly.edu/
2 Where is the department office? 0 | https://csc.calpoly.edu/contact/ | The department office is located in Building 14, Room 254.
2 Where is the CSC department office? 0 | https://csc.calpoly.edu/contact/ | The department office is located in Building 14, Room 254.
2 Where is the department located? 0 | https://csc.calpoly.edu/contact/ | The department office is located in Building 14, Room 254.
2 Where is the department office located? 0 | https://csc.calpoly.edu/contact/ | The department office is located in Building 14, Room 254.
2 How do I visit the office? 0 | https://csc.calpoly.edu/contact/ | You can visit the department office in Building 14, Room 254, from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday.
2 How can I contact the department? 0 | https://csc.calpoly.edu/contact/ | You can contact the department via phone 805-756-2824, fax 805-756-2956, email computer-science@calpoly.edu, or by visiting Building 14, Room 254, from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday. The mailing address is Computer Science Department, Building 14, Room 254, California Polytechnic State University, 1 Grand Avenue, San Luis Obispo, Ca 93407-0354.
2 How do I contact the department? 0 | https://csc.calpoly.edu/contact/ | You can contact the department via phone 805-756-2824, fax 805-756-2956, email computer-science@calpoly.edu, or by visiting Building 14, Room 254, from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday. The mailing address is Computer Science Department, Building 14, Room 254, California Polytechnic State University, 1 Grand Avenue, San Luis Obispo, Ca 93407-0354.
2 Show me the department's contact information. 0 | https://csc.calpoly.edu/contact/ | You can contact the department via phone 805-756-2824, fax 805-756-2956, email computer-science@calpoly.edu, or by visiting Building 14, Room 254, from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday. The mailing address is Computer Science Department, Building 14, Room 254, California Polytechnic State University, 1 Grand Avenue, San Luis Obispo, Ca 93407-0354.
2 What is the department office's phone number? 0 | https://csc.calpoly.edu/contact/ | Department office's phone number is 805-756-2824.
2 What is the department's phone number? 0 | https://csc.calpoly.edu/contact/ | Department office's phone number is 805-756-2824.
2 What is the office's number? 0 | https://csc.calpoly.edu/contact/ | Department office's phone number is 805-756-2824 and fax number is 805-756-2956.
2 What is the department office's fax number? 0 | https://csc.calpoly.edu/contact/ | Department office's fax number is 805-756-2956.
2 What is the department's email? 0 | https://csc.calpoly.edu/contact/ | Department's email address is computer-science@calpoly.edu.
2 Tell me department's email address. 0 | https://csc.calpoly.edu/contact/ | Department's email address is computer-science@calpoly.edu.
2 Tell me department email. 0 | https://csc.calpoly.edu/contact/ | Department's email address is computer-science@calpoly.edu.
2 What is the department's mailing address? 0 | https://csc.calpoly.edu/contact/ | Department's mailing address is Computer Science Department, Building 14, Room 254, California Polytechnic State University, 1 Grand Avenue, San Luis Obispo, Ca 93407-0354.
2 What is the office's mailing address? 0 | https://csc.calpoly.edu/contact/ | Department's mailing address is Computer Science Department, Building 14, Room 254, California Polytechnic State University, San Luis Obispo, Ca 93407-0354.
2 Who are department's staff? 0 | https://csc.calpoly.edu/faculty/ | Leanne Fiorentino (Administrative Support Coordinator), Ryan Lau (Administrative Analyst Specialist), and Bob Gafford (Operating Systems Analyst) are department staff.
2 Who works at the department office? 0 | https://csc.calpoly.edu/faculty/ | Leanne Fiorentino (Administrative Support Coordinator), Ryan Lau (Administrative Analyst Specialist), and Bob Gafford (Operating Systems Analyst) are department staff.
2 Whom can I contact? 0 | https://csc.calpoly.edu/faculty/ | You can contact our Administrative Support Coordinator Leanne Fiorentino for general departmental questions, via phone 805-756-2824 or email lfiorent@calpoly.edu.
2 Who is the department chair? 0 | https://csc.calpoly.edu/faculty | Dr. Ignatios Vakalis is our department chair.
2 How do I contact the department chair? 0 | https://csc.calpoly.edu/faculty/ | You can contact our department chair, Dr. Vakalis, via phone 805-756-6285 or email ivakalis@calpoly.edu.
2 Can I set up an appointment? 0 | https://csc.calpoly.edu/contact/ | Yes, you can contact the department via phone 805-756-2824, email computer-science@calpoly.edu, or by visiting Building 14, Room 254, from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday to set up an appoinment.
2 Can I get a tour? 0 | https://csc.calpoly.edu/contact/ | Please contact the department via phone 805-756-2824, email computer-science@calpoly.edu, or by visiting Building 14, Room 254, from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday to ask about the tour.
2 How many teachers are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are 40 faculty members in our department.
2 How many professors are under the department? 0 | https://csc.calpoly.edu/faculty/ | There are 24 professors in our department.
2 How many students does the department have? 0 | https://csc.calpoly.edu/about/ | There are 1,255 students in our department.
2 How many undergraduate students are there? 0 | https://csc.calpoly.edu/about/ | There are 1,180 undergraduate students in our department.
2 How many graduate students are there? 0 | https://csc.calpoly.edu/about/ | There are more than 75 graduate students in our department.
2 Who are department advisors? 0 | https://csc.calpoly.edu/iab/ | Mike Adams from Microsoft, Heidi Ammerlahn from Sandia National Laboratories, and Jason Anderson from Comp Three, Inc. are on our department's Industrial Advisory Board.
2 How can I contribute to the department? 0 | https://csc.calpoly.edu/giving/ | Thank you for the considerations! You can contact our department chair, Dr. Vakalis, via phone 805-756-6285 or email ivakalis@calpoly.edu to discuss donations.
2 How do I make a donation? 0 | https://csc.calpoly.edu/giving/ | Thank you for the considerations. You can contact our department chair, Dr. Vakalis, via phone 805-756-6285 or email ivakalis@calpoly.edu to discuss donations.
2 Can I make a donation? 0 | https://csc.calpoly.edu/giving/ | Thank you for the considerations! You can contact our department chair, Dr. Vakalis, via phone 805-756-6285 or email ivakalis@calpoly.edu to discuss donations.
2 Can I donate? 0 | https://csc.calpoly.edu/giving/ | Yes! Thank you for the considerations. You can contact our department chair, Dr. Vakalis, via phone 805-756-6285 or email ivakalis@calpoly.edu to discuss donations.
2 When is the department office open? 0 | https://csc.calpoly.edu/contact/ | Department office is open from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday.
2 What is the department office hours? 0 | https://csc.calpoly.edu/contact/ | Department office hours are from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday.
2 What are the department office hours? 0 | https://csc.calpoly.edu/contact/ | Department office hours are from 9:00 am to 12:00 pm and 1:00 pm to 4:00 pm, Monday through Friday.