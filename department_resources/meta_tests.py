import unittest, os

from department_resources.meta import DepartmentMeta


class MetaTests(unittest.TestCase):
    def test_process_base_queries(self):
        self.maxDiff = None
        meta = DepartmentMeta()
        result = meta.process_base_queries('./department_resources/project3_queries_test.txt')
        self.assertDictEqual(result, {'What is the department web site?' : [2,
                                                                            0,
                                                                            'https://csc.calpoly.edu/',
                                                                            'The department\'s web site is: https://csc.calpoly.edu/']})


if __name__ == '__main__':
    unittest.main()
