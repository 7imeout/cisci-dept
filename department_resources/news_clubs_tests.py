import unittest

from department_resources.news_clubs import DepartmentNewsClubs


class NewsClubsTests(unittest.TestCase):
    def test_init(self):
        g = DepartmentNewsClubs()

    def test_get_response(self):
        g = DepartmentNewsClubs()
        r = g.get_response('Is there a website for the latest news on the Computer Science department?')
        self.assertEqual(r[0], 1)
        self.assertEqual(r[1], DepartmentGeneral.SIG_NORMAL)
        self.assertEqual(r[2], 'Yes, here it is: https://csc.calpoly.edu/news/')
        r = g.get_response('DKFSDKJFSDFJKL')
        self.assertEqual(r[0], 0)
        self.assertEqual(r[1], DepartmentGeneral.SIG_UNKNOWN)
        self.assertEqual(r[2], 'I couldn\'t understand your question. Sorry!')

if __name__ == '__main__':
    unittest.main()
