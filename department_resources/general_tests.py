import unittest

from department_resources.general import DepartmentGeneral


class GeneralTests(unittest.TestCase):
    def test_init(self):
        g = DepartmentGeneral()

    def test_get_response(self):
        g = DepartmentGeneral()
        r = g.get_response('Can I donate?')
        self.assertEqual(r[0], 1)
        self.assertEqual(r[1], DepartmentGeneral.SIG_NORMAL)
        self.assertEqual(r[2], 'Yes! Thank you for the considerations. You can contact our department chair, [NAME], via phone [PHONE_NUM] or email [EMAIL] to discuss donations.')
        r = g.get_response('DKFSDKJFSDFJKL')
        self.assertEqual(r[0], 0)
        self.assertEqual(r[1], DepartmentGeneral.SIG_UNKNOWN)
        self.assertEqual(r[2], 'I couldn\'t understand your question. Sorry!')

if __name__ == '__main__':
    unittest.main()
