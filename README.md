# README #

This is a repository for the Department module, which is a part of CiSCi Chatbot class project.

### What is this repository for? ###

* This repo holds the exact directory / file structures for the Department module of CiSCi Chatbot.

### Course ###

* CPE 466
* Knowledge and Discovery of Data
* Spring 2016
* California Polytechnic State University
* San Luis Obispo, CA

### Contributors ###

* Mike Ryu
* Eric Tran
* Kishan Patel
* Paul Sullivan (Group Representative)
* Mallika Potter