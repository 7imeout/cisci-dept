import nltk, re, sys, math, operator

from random import shuffle
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords

from department_resources.general import DepartmentGeneral
from department_resources.events import DepartmentEvents
from department_resources.buildings import DepartmentBuildings
from department_resources.staff_misc import DepartmentStaffMisc
from department_resources.news_clubs import DepartmentNewsClubs
from department_resources.labs import DepartmentLabs


class Department:
    moduleContributors = ['Mike Ryu', 'Eric Tran', 'Kishan Patel', 'Paul Sullivan', 'Mallika Potter']
    moduleName = "Department"
    moduleDescription = "This module provides departmental information about the Cal Poly's computer science department. " \
                        "The data repository is located on the Frank server under /mnt/cisci/modules/department_module. " \
                        "Last release on: Wednesday, June 9 at 7:20 PM by Mike Ryu"

    numberOfResponses = 0

    RATING_INDEX = 0
    SIGNAL_INDEX = 1
    RESPONSE_INDEX = 2
    SUBMODULE_INDEX = 3

    def __init__(self):
        """
        Instantiates all sub-modules and prepares Department object to be able to start answering questions.
        Call self.update() (see below) to update source information here, if necessary.
        """
        self.debug = False
        self.demo_mode = False

        self.dataStore = []  # you may read data from a pickeled file or other sources
        self.subModules = []
        self.subModules.append(DepartmentGeneral('./department_resources/general_r/base_queries_0_only.txt'))
        self.subModules.append(DepartmentEvents('./department_resources/events_r/canned_queries.txt'))
        self.subModules.append(DepartmentBuildings('./department_resources/buildings_r/base_queries.txt'))
        self.subModules.append(DepartmentStaffMisc('./department_resources/staff_misc_r/base_queries.txt'))
        self.subModules.append(DepartmentNewsClubs('./department_resources/news_clubs_r/canned_queries.txt'))
        self.subModules.append(DepartmentLabs('./department_resources/lab_info_r/canned_queries.txt'))

        self.currentQuery = None
        self.history = []

        self.bestResponse = None
        self.exactMatch = -1
        self.responses = []

        self.vocabSet = set()
        self.trainingSet = []
        self.classifier = None

        self.train_classifier()

    def id(self):
        """
        Used by dispatcher to simply get ID info of our module.
        :return: module name and module description
        """
        return [self.moduleName, self.moduleDescription]

    def credits(self):
        """
        Used by dispatcher to display the contributors to this module.
        :return: list of contributors' names
        """
        return ', '.join(self.moduleContributors)

    def update(self):
        """
        Updates the source information used to answer questions (i.e. department chair's name)
        If necessary, trigger individual sub-modules' update routines here.
        :return: True if successful, False otherwise (?)
        """
        # some update procedure dealing with dataStore,
        # if items need to be crawled and saved, this is the place to do that
        status = True
        count = 0
        for sm in self.subModules:
            status &= sm.update()
            if status and self.demo_mode:
                count += 1
                progress = count / len(self.subModules) * 100
                print('{:02.1f}%'.format(progress), end=" ")
                sys.stdout.flush()
        return status

    def getRating(self, query, history=[]):  #
        """
        Returns rating based on query and (if you wish) the history
        :param query: current query
        :param history: optional; list of the query / response history (?)
        :return: rating of confidence regarding our answer to the current query
        """
        if len(query) == 0:
            return 0.0
        else:
            query = re.sub('\?', '', query)
            query_words = query.strip().split()
            max = 0.0
            for sm in self.subModules:
                count = 0
                for word in query_words:
                    if word.lower() in sm.vocab:
                        count += 1
                rating = count / len(query_words)
                if rating > max:
                    max = rating
            return max

    def response(self, query, history=[]):
        """
        Returns the response to the given query.
        :param query: current query
        :param history: optional; list of the query / response history (?)
        :return: our best response for the query
        """
        query = re.sub('\?', '', query)
        rating = self.getRating(query, history)
        if (not self.responses) or (self.currentQuery != query):
            self.ask_submodules(query, history)
            self.ask_best_submodule(query, history)

        if len(query) > 0:  # signals can be "Normal", "Error", "Question", "Unknown" or "End"
            if (self.bestResponse[self.RATING_INDEX] == 0.0 and self.responses[0][self.RATING_INDEX] == 0.0):
                response_string = "Sorry, I couldn't understand your question. Try again?"
            elif (self.bestResponse[self.RATING_INDEX] > self.responses[0][self.RATING_INDEX]):
                response_string = self.bestResponse[self.RESPONSE_INDEX]
            else:
                response_string = self.responses[0][self.RESPONSE_INDEX]
            signal = self.responses[0][self.SIGNAL_INDEX]
        else:
            response_string = "You didn't ask any question. Try again?"
            signal = "Error"

        self.history.append(self.currentQuery)
        self.currentQuery = query

        return [rating, signal, response_string]

    def train_classifier(self):
        for sm in self.subModules:
            for vocab in sm.vocab:
                self.vocabSet.add(vocab)

        trainingSet = []
        for sm in self.subModules:
            features = {}
            for word in self.vocabSet:
                if word in sm.vocab:
                    features[word] = True
                else:
                    features[word] = False
            trainingSet.append((features, self.subModules.index(sm)))

        self.trainingSet = trainingSet
        self.classifier = nltk.NaiveBayesClassifier.train(self.trainingSet)
        if (self.debug):
            print(self.classifier.show_most_informative_features(10))

    def ask_best_submodule(self, query, history):
        stemmer = SnowballStemmer('english', ignore_stopwords=True)
        query = query.lower().strip()
        query_words = [re.sub(r'\W', '', word) for word in query.split()]
        query_words.extend(list(map(lambda word: stemmer.stem(word), query_words)))

        features = {}
        for word in query_words:
            if word in self.vocabSet:
                features[word] = True
            else:
                features[word] = False
        result = self.classifier.classify(features)

        doc_scores = self.get_doc_scores(query_words)

        if result < len(self.subModules) and self.debug:
            print('Query seems to belong in a sub-module:',
                  type(self.subModules[result]).__name__, "(NB)",
                  type(self.subModules[doc_scores[0][0]]).__name__, "(TF-IDF)")

        self.bestResponse = self.subModules[result].get_response(query, history)

        if (self.bestResponse[self.RATING_INDEX] == 1.0):
            self.responses.append(self.bestResponse)
            self.exactMatch = result
        else:
            self.exactMatch = -1

    def ask_submodules(self, query, history):
        self.responses = []
        for i in range(len(self.subModules)):
            if (i != self.exactMatch):
                r = self.subModules[i].get_response(query, history)
                self.responses.append((r[self.RATING_INDEX], r[self.SIGNAL_INDEX], r[self.RESPONSE_INDEX],
                                       type(self.subModules[i]).__name__))
        self.responses = sorted(self.responses, key=lambda x: x[0], reverse=True)
        self.print_responses()

    def print_responses(self):
        if self.debug:
            if not self.responses:
                'No responses available'
            else:
                print("NB Classification Results:")
                for response in self.responses:
                    print("Rating {:1.3f}, {:20}: {:.40} ..."
                          .format(response[self.RATING_INDEX], response[self.SUBMODULE_INDEX],
                                  response[self.RESPONSE_INDEX]))
                print()

    def get_most_freq_wc(self, document):
        words = {}
        for word in document:
            if word in words:
                words[word] += 1
            else:
                words[word] = 1
        freq_word = max(words, key=lambda count: words[count])
        freq_count = words[freq_word]
        assert max(words.values()) == freq_count
        return freq_word, freq_count

    def get_norm_tf(self, term_freq, normalizer):
        return term_freq / normalizer

    def get_idf(self, all_freq):
        return math.log((len(self.subModules) + 1) / (all_freq + 1))

    def get_doc_freqs(self, term):
        all_freqs = 0
        for sm in self.subModules:
            all_freqs += 1 if term.lower() in sm.vocab else 0
        return all_freqs

    def get_normalizers(self):
        normalizers = {}
        for sm in self.subModules:
            normalizers[self.subModules.index(sm)] = self.get_most_freq_wc(sm.all_words)[1]
        return normalizers

    def get_doc_scores(self, terms):
        normalizers = self.get_normalizers()
        doc_scores = {}
        for sm in self.subModules:
            total = 0
            for term in terms:
                if term not in stopwords.words('english'):
                    doc = sm.all_words
                    norm = normalizers[self.subModules.index(sm)]
                    tf = self.get_norm_tf(doc.count(term.lower()), norm)
                    idf = self.get_idf(self.get_doc_freqs(term))
                    total += tf * idf
            doc_scores[self.subModules.index(sm)] = total
        sorted_docs = sorted(doc_scores.items(), key=operator.itemgetter(1), reverse=True)
        if self.debug:
            print("TF-IDF Classification Results:")
            for entry in sorted_docs:
                print("Rating {:1.3f}, {:20}".format(entry[1], type(self.subModules[entry[0]]).__name__))
            print()
        return sorted_docs

    def run_self_test(self, size):
        base_queries = []
        for sm in self.subModules:
            base_queries.extend(sm.questions.keys())
        shuffle(base_queries)
        test_list = base_queries[:size]
        rating_sum = 0
        count = 0
        for q in test_list:
            rating_sum += self.response(q)[0]
            print('.' if count % (size / 5) == 0 else '', end='')
            sys.stdout.flush()
            count += 1
        return rating_sum / size * 100


def test():
    """
    Stand-alone interactive demo of our module.
    """
    test_list_size = 50
    myModule = Department()
    if '-d' in sys.argv:
        print('Running in debug mode')
        myModule.debug = True
        test_list_size = 10

    print("module name:", myModule.id()[0])
    print("module description:", myModule.id()[1])
    print("credits:", myModule.credits())

    print("\nRUNNING SELF TEST", end=' ')
    sys.stdout.flush()
    print(' COMPLETED, SCORE: {:02.2f}%\n'.format(myModule.run_self_test(test_list_size)))

    query = ""
    response = []
    print("interactive demo (Y/N)?", end=" ")
    KB = input()
    if len(KB) > 0 and KB[0] in ['y', 'Y']:
        myModule.demo_mode = True
        print("re-scrape from web to update JSON (Y/N)?", end=" ")
        update = input()
        if len(update) > 0 and update[0] in ['y', 'Y']:
            print("\nUPDATING DATA", end=" ")
            sys.stdout.flush()
            result = myModule.update()
            print("SUCCESS!" if result else "FAILED (check network access status)")
        history = [[query, response]]
        exitflag = False
        while not exitflag:  # query.strip() not in ['quit', 'Quit', 'exit', 'Exit']:
            print("\nHow can I help you? (\"quit\" to exit)", end=" ")
            query = input().strip().lower()
            if query in ['quit', 'exit']:
                exitflag = True
                print("Bye!")
            else:
                response = myModule.response(query, history)
                history.append([query, response])
                if response[1] is "Error":
                    print('[Rating: {:01.3f}]'.format(response[0]), response[2])
                    continue
                else:
                    print('[Rating: {:01.3f}]'.format(response[0]), response[2])


if __name__ == "__main__":
    test()
